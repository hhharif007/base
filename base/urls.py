# urls.py
from django.urls import path
from .views import BaseModelAPIView

urlpatterns = [
    path('base-models/', BaseModelAPIView.as_view(), name='base-model-list-create'),
    path('base-models/<int:pk>/', BaseModelAPIView.as_view(), name='base-model-retrieve-update-delete'),
]
