# views.py
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .models import BaseModel
from .serializers import BaseModelSerializer

class BaseModelAPIView(APIView):
    def get(self, request):
        base_models = BaseModel.objects.all()
        serializer = BaseModelSerializer(base_models, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = BaseModelSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, pk):
        base_model = BaseModel.objects.get(pk=pk)
        serializer = BaseModelSerializer(base_model, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        base_model = BaseModel.objects.get(pk=pk)
        base_model.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
