from django.db import models

# Create your models here.


class BaseModel(models.Model):
    name = models.CharField(max_length=255,null=True)
    qualifications = models.CharField(max_length=255,null=True)